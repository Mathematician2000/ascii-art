# asciify

Create amazing ASCII arts on-the-fly with `asciify-it` Python package!

## Features
- Algorithm for image transformation into an ASCII art!
- Python library `asciify` with command line interface (powered by `click`)
- `Flask` server application
- Deployed on [Heroku](https://asciify-it.herokuapp.com)

## Dependencies
- click
- numba
- numpy
- Pillow
- scipy
- tqdm

## Codestyle linters and test frameworks
The library has been fully checked and tested with the following tools:
- flake8
- mypy
- pydocstyle
- pytest

## Interface
CLI interface is described in the [examples](https://ascii-art.readthedocs.io/en/latest/examples.html) documentation section.
This is how you can use `asciify` CLI tool:

`$ asciify mylittlepony.png -w 45 -h 43 -o result.txt -f path/to/arial.ttf -s 48 -q`

Here are some tips on how you can use our [heroku web-application](https://asciify-it.herokuapp.com).

- You are free to choose any interface language! (But only English and Russian are supported at the moment...)
- There's another [page](https://asciify-it.herokuapp.com/asciify) for you to *asciify* you image right away!
- This is how it looks in the browser:

![An image is missing?](misc/server_interface.png "This is how it looks in the browser")

## Authors
- [AArtur](https://gitlab.com/AAArtur)
- [Mathematician2000](https://gitlab.com/Mathematician2000)

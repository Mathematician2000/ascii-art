Quick start
===========

Installation/Usage
******************

To install ``asciify`` simply run::

    pip install asciify-it

Dependencies:

* click
* numba
* numpy
* Pillow
* tqdm

Examples
********
Consider this little Python code snippet:

.. code-block:: python
    :linenos:

    from asciify import AsciiManager
    from PIL import Image

    # Minimal example
    image = Image.open('mylittlepony.png')
    manager = AsciiManager()
    ascii_art = manager.transform(image, (100, 130))  # width x height
    print(ascii_art)

The following ASCII art is printed to stdout:

.. literalinclude:: output.txt

You are also free to use ``asciify`` CLI tool as follows:

.. code-block:: console

    > regular call
    $ asciify mylittlepony.png

    > customize output ASCII art width and height
    $ asciify mylittlepony.png --width 45 --height 43

    > redirect output to a file
    $ asciify mylittlepony.png --output result.txt

    > change inner font for similar glyph selection
    $ asciify mylittlepony.png --fontname path/to/arial.ttf --fontsize 48

    > shut the hell up
    $ asciify mylittlepony.png --quiet

    > open the hell up
    $ asciify mylittlepony.png --verbose

    > contract options
    $ asciify mylittlepony.png -v

    > all together
    $ asciify mylittlepony.png -w 45 -h 43 -o result.txt -f path/to/arial.ttf -s 48 -q

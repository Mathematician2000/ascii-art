Introduction
============

Create amazing ASCII arts on-the-fly with ``asciify`` package!

This package provides algorithm implementation for transforming an image into an awesome ASCII art.
It can be used as a regular Python library or utilized via ``asciify`` CLI tool.

Have fun!

Main features
*************

+ AWESOME ASCII arts have never been so easy to create in Python!
+ Fast numpy operations underlying algorithm implementation
+ Extremely low space consumption
+ AMAZING examples presented :doc:`here <examples>` for you!

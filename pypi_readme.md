# asciify

Create amazing ASCII arts on-the-fly with `asciify` Python package!

## Features
- Algorithm for image transformation into an ASCII art!
- Python library `asciify` with command line interface (powered by `click`)
- `Flask` server application
- Deployed on [Heroku](https://asciify-it.herokuapp.com)

## Dependencies
- click
- numba
- numpy
- Pillow
- scipy
- tqdm

## Quick start
To install `asciify` simply run

`pip install asciify-it`

Now you can use `asciify` CLI tool as follows:

`$ asciify mylittlepony.png -w 45 -h 43 -o result.txt -f path/to/arial.ttf -s 48 -q`

More info can be found on the [documentation](https://ascii-art.readthedocs.io/en/latest) page.

## Authors
- [AArtur](https://gitlab.com/AAArtur)
- [Mathematician2000](https://gitlab.com/Mathematician2000)

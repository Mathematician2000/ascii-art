"""Flask app creation."""

import os
from typing import Any

from flask import Flask, request, session
from flask_babel import Babel

import config  # noqa
from .asciifier import AsciifyImageModel


app = Flask(__name__)
app.config.from_object(os.environ.get('FLASK_ENV', 'config.TestingConfig'))

babel = Babel(app)

model = AsciifyImageModel()


@babel.localeselector
def get_locale() -> Any:
    """Get current locale for babel translations."""

    lang = request.args.get('lang')
    if lang is not None:
        session['lang'] = lang
    return session.get('lang', app.config['BABEL_DEFAULT_LOCALE'])


from . import views  # noqa


__all__ = [
    'app',
    'model',
]

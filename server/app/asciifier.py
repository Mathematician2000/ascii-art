"""Class for asciify operations."""

from typing import Any, Tuple
from typing_extensions import TypeAlias

from asciify import AsciiManager
from PIL import Image


ImageType: TypeAlias = Image.Image


class AsciifyImageModel:
    """Model for ASCII art transformation."""

    def __init__(self, *args: Any, **kwargs: Any):
        """Initialize ASCII art transformer."""

        self.manager = AsciiManager(*args, **kwargs)

    def transform(self, image: ImageType, output_size: Tuple[int, int]) -> str:
        """
        Transform an image into an awesome ASCII art of given size.

        :param image: Input image to process.
        :param output_size: Output ASCII art size in the form of (width, height).
        :return: string containing ASCII art.
        """

        return self.manager.transform(image, output_size)

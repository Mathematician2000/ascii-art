"""Flask app forms."""

from flask_babel import lazy_gettext
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField, FileRequired
from wtforms import IntegerField, SubmitField
from wtforms.validators import InputRequired, NumberRange


class AsciifyImageForm(FlaskForm):
    """Submit form for ASCII art transformation."""

    image = FileField(lazy_gettext('Pick an image to transform!'), validators=[
        FileRequired(),
        FileAllowed(['jpg', 'jpeg', 'png', 'gif', 'tiff'], lazy_gettext('Images only!')),
    ])

    width = IntegerField(lazy_gettext('ASCII art width'), default=128, validators=[
        InputRequired(),
        NumberRange(min=1, max=1000),
    ])

    height = IntegerField(lazy_gettext('ASCII art height'), default=96, validators=[
        InputRequired(),
        NumberRange(min=1, max=1000),
    ])

    submit = SubmitField(lazy_gettext('Asciify!'))

"""Flask app views."""

from pathlib import Path
import subprocess

from flask import flash, render_template, redirect, request
from flask_babel import get_locale, gettext
from PIL import Image

from app import app, model
from .forms import AsciifyImageForm


def pybabel_compile_on_demand():
    """Compile babel translations in case they are missing."""

    locale = get_locale()
    cwd = Path(__file__).parent
    mo_path = cwd / f'translations/{locale}/LC_MESSAGES/messages.mo'
    if not mo_path.exists():
        po_path = cwd / f'translations/{locale}/LC_MESSAGES/messages.po'
        subprocess.run([
            'pybabel', 'compile',
            '-d', cwd / 'translations',
            '-i', po_path, '-o', mo_path,
        ])


@app.route('/')
def page_home():
    """App home page view."""

    pybabel_compile_on_demand()
    return render_template('home.html')


@app.route('/asciify/', methods=['POST', 'GET'])
def page_asciify():
    """App asciify page view."""

    pybabel_compile_on_demand()
    form = AsciifyImageForm()
    if form.csrf_token.errors:
        flash(gettext('You have sent an invalid CSRF token'), 'error')
    if form.validate_on_submit():
        file = form.image.data
        image = Image.open(file.stream)
        try:
            output_size = (form.width.data, form.height.data)
            result = model.transform(image, output_size)
            flash(gettext('Successfully processed an image!'), 'info')
        except Exception as err:
            flash(str(err), 'error')
            return redirect(request.url)
        return render_template('result.html', result=result)
    return render_template('asciify.html', form=form)


@app.route('/about/')
def page_about():
    """App 'about' page view."""

    pybabel_compile_on_demand()
    return render_template('about.html')

"""Flask app configuration."""

import os


app_dir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:
    """Base class for flask app configs."""

    SECRET_KEY = os.environ.get('SECRET_KEY', 'A SECRET KEY')
    UPLOAD_FOLDER = '/static/uploads'
    LANGUAGES = {
        'en': 'English',
        'ru': 'Russian',
    }
    BABEL_DEFAULT_LOCALE = 'en'


class DevelopementConfig(BaseConfig):
    """Flask app config for development environment."""

    DEBUG = True


class TestingConfig(BaseConfig):
    """Flask app config for testing environment."""

    DEBUG = True


class ProductionConfig(BaseConfig):
    """Flask app config for production environment."""

    DEBUG = False

"""Main entry point for server application."""

import os

from waitress import serve

from app import app


HOST = os.environ.get('HOST', '0.0.0.0')
PORT = int(os.environ.get('PORT', 33507))


if __name__ == '__main__':
    print('Ready to serve!')
    serve(app, host=HOST, port=PORT)

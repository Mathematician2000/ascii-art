from warnings import catch_warnings, simplefilter

import numpy as np
from numpy.testing import assert_array_equal
from PIL import Image, ImageFont, ImageOps
import pytest

from asciify import AsciiManager, draw_char, get_tiles, score_similarity


def test_draw_char() -> None:
    font = ImageFont.truetype('tests/courier.ttf', 8)
    image = draw_char(font, 'F')
    assert image.size == (5, 7), 'Wrong image size'

    arr_true = np.array([
        [255, 255, 255, 255, 255],
        [255, 255, 255, 255, 255],
        [228, 150, 171, 171, 196],
        [255, 171, 251, 253, 194],
        [255, 139, 134, 224, 255],
        [255, 171, 237, 246, 255],
        [229, 115, 180, 255, 255],
    ], dtype=np.uint8)
    try:
        assert_array_equal(np.array(image), arr_true)
    except AssertionError:
        raise AssertionError('Wrong char glyph')


def test_get_tiles() -> None:
    image = Image.new('RGB', (10, 15), 'yellow')

    with catch_warnings():
        simplefilter('ignore', category=FutureWarning)

        tiles = [list(row) for row in get_tiles(image, (2, 3))]
        arr = np.asarray(tiles, dtype=object)
        assert arr.shape == (5, 5), 'Wrong tile sheet shape'

        tiles = [list(row) for row in get_tiles(image, (2, 4))]
        arr = np.asarray(tiles, dtype=object)
        assert arr.shape == (4, 5), 'Wrong tile sheet shape'
        assert arr[-1, -1].size == (2, 4), 'Wrong tile size'


def test_score_similarity() -> None:
    lhs = np.array([[0, 1], [2, 3]])
    rhs = np.array([[1, 2], [3, 4]])
    assert score_similarity(lhs, rhs) == -2.0, 'Wrong similarity score'


class TestManager:
    def test_init(self) -> None:
        manager = AsciiManager(fontsize=42, verbose=False)
        assert manager.chars['F'].size == (25, 35), 'Wrong glyph size'

        with pytest.raises(OSError):
            manager = AsciiManager(fontname='mylovelyfont.ttf')

    def test_find_closest_char(self) -> None:
        manager = AsciiManager(fontsize=42, verbose=False)
        resized_chars = {
            char: np.array(glyph.resize((10, 15)))
            for char, glyph in manager.chars.items()
        }

        image = ImageOps.grayscale(Image.new('RGB', (10, 15), 'yellow'))
        result = manager._find_closest_char(image, resized_chars)
        assert len(result) == 1, 'Wrong closest char output'

    def test_get_tile_size(self) -> None:
        manager = AsciiManager(verbose=False)
        assert manager._get_tile_size((25, 35), (7, 4)) == ((3, 8), (21, 32)), 'Wrong tile/window size'
        assert manager._get_tile_size((25, 35), (5, 7)) == ((5, 5), (25, 35)), 'Wrong tile/window size'
        assert manager._get_tile_size((25, 35), (4, 4)) == ((6, 8), (24, 32)), 'Wrong tile/window size'
        assert manager._get_tile_size((100, 200), (100, 200)) == ((1, 1), (100, 200)), 'Wrong tile/window size'

        for size in [(1, 201), (101, 1), (101, 201), (0, 200), (100, 0), (100, -1)]:
            with pytest.raises(ValueError):
                manager._get_tile_size((100, 200), size)

    def test_transform(self) -> None:
        image = Image.new('RGB', (10, 15), 'yellow')
        manager = AsciiManager(fontsize=42, verbose=False)
        text = manager.transform(image, (10, 15))
        assert len(text) == 164, 'Wrong output length'
        assert len(text.splitlines()) == 15, 'Wrong number of output lines'
        assert np.array([list(xs) for xs in text.splitlines()]).shape == (15, 10), 'Wrong output shape'

        for size in [(10, 16), (11, 15), (11, 160), (10, 0), (0, 15), (-1, 15)]:
            with pytest.raises(ValueError):
                manager.transform(image, size)

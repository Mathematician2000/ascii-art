from click.testing import CliRunner
import numpy as np
from PIL import Image

from asciify import asciify


class TestCli:
    def test_asciify(self) -> None:
        runner = CliRunner()
        with runner.isolated_filesystem():
            image = Image.new('RGB', (200, 200), 'green')
            image.save('img.png')
            result = runner.invoke(asciify, ['img.png', '-q', '-w', '10', '-h', '5'])

            assert result.exit_code == 0, 'Smth went wrong in asciify script'
            assert len(result.output) == 5 * 11, 'Wrong output length'

            lines = result.output.splitlines()
            assert len(lines) == 5, 'Wrong number of output lines'
            assert np.array([list(xs) for xs in lines]).shape == (5, 10), 'Wrong output shape'

    def test_default_sizes(self) -> None:
        runner = CliRunner()
        with runner.isolated_filesystem():
            image = Image.new('RGB', (20, 30), 'green')
            image.save('img.png')

            result = runner.invoke(asciify, ['img.png', '-q'])
            assert result.exit_code == 0, 'Smth went wrong in asciify script'
            assert len(result.output) == 30 * 21, 'Wrong output length'
            lines = result.output.splitlines()
            assert len(lines) == 30, 'Wrong number of output lines'
            assert np.array([list(xs) for xs in lines]).shape == (30, 20), 'Wrong output shape'

            result = runner.invoke(asciify, ['img.png', '-q', '-w', '10'])
            assert result.exit_code == 0, 'Smth went wrong in asciify script'
            assert len(result.output) == 15 * 11, 'Wrong output length'
            lines = result.output.splitlines()
            assert len(lines) == 15, 'Wrong number of output lines'
            assert np.array([list(xs) for xs in lines]).shape == (15, 10), 'Wrong output shape'

            result = runner.invoke(asciify, ['img.png', '-q', '-h', '10'])
            assert result.exit_code == 0, 'Smth went wrong in asciify script'
            assert len(result.output) == 10 * 7, 'Wrong output length'
            lines = result.output.splitlines()
            assert len(lines) == 10, 'Wrong number of output lines'
            assert np.array([list(xs) for xs in lines]).shape == (10, 6), 'Wrong output shape'

    def test_wrong_input(self) -> None:
        runner = CliRunner()
        with runner.isolated_filesystem():
            result = runner.invoke(asciify, ['non-existing-path.jpg'])
            assert result.exit_code != 0, 'How did you process non-existing image??'

    def test_wrong_size(self) -> None:
        runner = CliRunner()
        with runner.isolated_filesystem():
            image = Image.new('RGB', (200, 200), 'yellow')
            image.save('img.png')

            for width in [0, -42, 201]:
                result = runner.invoke(asciify, ['img.png', '-w', str(width)])
                assert result.exit_code != 0, 'How did you process wrong output width??'

            for height in [0, -42, 201]:
                result = runner.invoke(asciify, ['img.png', '-h', str(height)])
                assert result.exit_code != 0, 'How did you process wrong output height??'

    def test_wrong_font(self) -> None:
        runner = CliRunner()
        with runner.isolated_filesystem():
            image = Image.new('RGB', (200, 200), 'yellow')
            image.save('img.png')

            result = runner.invoke(asciify, ['img.png', '-f', 'holymoly.ttf'])
            assert result.exit_code != 0, 'How did you process non-existing font??'

    def text_huge_image(self) -> None:
        runner = CliRunner()
        with runner.isolated_filesystem():
            image = Image.new('RGB', (2000, 2000), 'red')
            image.save('img.png')
            result = runner.invoke(asciify, ['img.png', '-q', '-w', '200', '-h', '100'])

            assert result.exit_code == 0, 'Smth went wrong in asciify script'
            assert len(result.output) == 100 * 201, 'Wrong output length'

            lines = result.output.splitlines()
            assert len(lines) == 100, 'Wrong number of output lines'
            assert np.array([list(xs) for xs in lines]).shape == (100, 200), 'Wrong output shape'
